FROM ericvh/arm64-ubuntu-dev
MAINTAINER Eric Van Hensbergen <ericvh@gmail.com>
RUN DEBIAN_FRONTEND=noninteractive apt-get install -q -y -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" libopenmpi-dev openmpi-bin openmpi-common gfortran pciutils pkg-config gfortran hpcc tau papi-tools libpapi-dev valgrind sysstat openssh-server systemtap-common ltrace strace && apt-get clean
